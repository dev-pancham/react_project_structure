import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';

const Header  = () => (
  <AppBar position="static" color="inherit">
    <Toolbar>
      <h2>Cuisine Crafter</h2>
    </Toolbar>
  </AppBar>
)
export default Header;
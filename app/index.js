import React from 'react';
import ReactDOM from 'react-dom';
import { withStyles } from '@material-ui/core/styles';
import Header from './components/Header';
const styles = {
  root: {
    flexGrow: 1,
  },
};
const Index = ()=>(
  <div>
    <Header/>
    <p>Home</p>
  </div>
)
const App = withStyles(styles)(Index)

ReactDOM.render(
  <App/>
  ,document.getElementById('app'))
